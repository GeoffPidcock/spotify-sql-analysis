/*
QUESTION 2. 
Are people listening to the very same top ranking songs on countries far away from each other?

Approach: 
- create an index of countries and distances using a self-join and lat lon calcs
-- ensure this index is populated with combinations, not permutations
- calculate distance between countries using haversine formula 
-- check answer using this wonderful resource - https://andrew.hedges.name/experiments/haversine/
- use materialized views over common table expressions to enhance performance
- test out country comparisons using correlated sub-queries (see rubbish)
- insert into table row result set (or sample, pending performance)
*/

-- defining index of countries and distances
create materialized view sampled_data as
	select *
	from data_raw raw
	where 
		raw.region not in ('ee','global','gr','gt','hn','hu','is','jp','lt','lu','lv','pa','py','sk','sv','uy') -- regions with incomplete data
		and raw.url is not null
with data;

create materialized view distinct_country_list as 
	select distinct region as country
	from sampled_data 
--	from data_raw raw -- #TODO update this materialized view to reference sampled_data
--	where 
--		raw.region not in ('ee','global','gr','gt','hn','hu','is','jp','lt','lu','lv','pa','py','sk','sv','uy') -- regions with incomplete data
--		and raw.url is not null
with data;
		
create materialized view country_distance as 
	select 
		c1.country_code as from_country_code
		,c1.country_name as from_country_name
		,c1.anglosphere as from_country_in_anglosphere
		,c1.latitude as from_lat
		,c1.longitude as from_lon
		,c2.country_code as to_country_code
		,c2.country_name as to_country_name
		,c2.anglosphere as to_country_in_anglosphere
		,c2.latitude as to_lat
		,c2.longitude as to_lon	
		,2 * 6371 * asin(sqrt((sin(radians((c2.latitude - c1.latitude) / 2))) ^ 2 + cos(radians(c1.latitude)) * cos(radians(c2.latitude)) * (sin(radians((c2.longitude - c1.longitude) / 2))) ^ 2)) as distance_between_estimate_km
	from
		data_countries_location_lang c1 inner join
		data_countries_location_lang c2 on c1.country_code < c2.country_code
	where
		c1.country_code in (select * from distinct_country_list)
		and c2.country_code in (select * from distinct_country_list) -- got 1406 results, which means we have purmutations, not combinations - #DEDUP #DEBUG
with data;

-- logic test 
--select * from country_distance
--where from_country_name = 'Australia' or to_country_name = 'Australia'
--order by distance_between_estimate_km


-- Create table of distances and top 50 track intersections
-- > due to compute limits	

drop table if exists q2_country_distance_with_anglo;
create table q2_country_distance_with_anglo as

select
	concat(from_country_code,'-',to_country_code) as from_to_country_index
	,from_country_code
	,to_country_code
	,from_country_in_anglosphere
	,to_country_in_anglosphere
	,distance_between_estimate_km
	,(
	--
		/*
		The following takes ~10 seconds per row in result set - if applied to all rows, it would take ~4 hours to compute.
		Which is why we're filtering for only combinations with the 5 anglosphere countries. 
		*/
		select 
			count(country_intersect.track_name) as top_50_intersect_count
		from ( 
			select 
				country_from_agg.track_name as track_name
			from (			
				select 
					sampled_data.region
					,sampled_data.track_name
					,sum(sampled_data.streams) as sum_stream_count
					,rank() over (partition by sampled_data.region order by sum(sampled_data.streams) desc) as agg_position
				from
					sampled_data
				where
					sampled_data.region = country_distance.from_country_code
					-- sampled_data.region = 'au'
				group by
					sampled_data.region
					,sampled_data.track_name
				) country_from_agg
			where 
				country_from_agg.agg_position <= 50 -- limiting to top 50 - AU for example has 1255 distinct tracks in the top 200 over the course of a year

			-- intersect with top 50 songs for to_country
			intersect

			select 
				country_to_agg.track_name as count_track
			from (
				select 
					sampled_data.region
					,sampled_data.track_name
					,sum(sampled_data.streams) as sum_stream_count
					,rank() over (partition by sampled_data.region order by sum(sampled_data.streams) desc) as agg_position
				from
					sampled_data
				where
					sampled_data.region = country_distance.to_country_code
					-- sampled_data.region = 'de' -- debug
				group by
					sampled_data.region
					,sampled_data.track_name
				) country_to_agg
			where 
				country_to_agg.agg_position <= 50 
		) country_intersect
	) as top_50_intersect_count
from country_distance
where
	from_country_in_anglosphere = True
	or from_country_in_anglosphere = True
;

-- OPTIONAL
-- run if you have an estimated three hours to burn - will compute intersects for all .

drop table if exists q2_country_distance_all;
create table q2_country_distance_all as

select
	concat(from_country_code,'-',to_country_code) as from_to_country_index
	,from_country_code
	,to_country_code
	,from_country_in_anglosphere
	,to_country_in_anglosphere
	,distance_between_estimate_km
	,(
	--
		/*
		The following takes 10 seconds per row - if applied to the whole row set, it would take ~4 hours to compute.
		Which is why we'll sample later. 
		*/
		select 
			count(country_intersect.track_name) as top_50_intersect_count
		from ( 
			select 
				country_from_agg.track_name as track_name
			from (			
				select 
					sampled_data.region
					,sampled_data.track_name
					,sum(sampled_data.streams) as sum_stream_count
					,rank() over (partition by sampled_data.region order by sum(sampled_data.streams) desc) as agg_position
				from
					sampled_data
				where
					sampled_data.region = country_distance.from_country_code
					-- sampled_data.region = 'au'
				group by
					sampled_data.region
					,sampled_data.track_name
				) country_from_agg
			where 
				country_from_agg.agg_position <= 50 -- limiting to top 50 - AU for example has 1255 distinct tracks in the top 200 over the course of a year

			-- intersect with top 50 songs for to_country
			intersect

			select 
				country_to_agg.track_name as count_track
			from (
				select 
					sampled_data.region
					,sampled_data.track_name
					,sum(sampled_data.streams) as sum_stream_count
					,rank() over (partition by sampled_data.region order by sum(sampled_data.streams) desc) as agg_position
				from
					sampled_data
				where
					sampled_data.region = country_distance.to_country_code
					-- sampled_data.region = 'de' -- debug
				group by
					sampled_data.region
					,sampled_data.track_name
				) country_to_agg
			where 
				country_to_agg.agg_position <= 50 
		) country_intersect
	) as top_50_intersect_count
from country_distance
;

	
