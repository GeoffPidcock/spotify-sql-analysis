/*
QUESTION 3.
How long does a top ranking song takes to get into the ranking of neighbor countries?

Approach:
- Date encode data
- Define neighbour... :) 
-- At least from Q2, we know AU-NZ, AU-GB, CA-US, GB-IE, are closely related pairs with high correspondance in their global top 50's.
-- Additionally, AU-FR is low correspondance, and AU-HK is medium.
- Which songs? we can take a look at intersecting top 10's or event top 5's maybe, and track how they go.  
- What level of date aggregation - will daily break the BI tool? May we have to move to weekly?
- and Extra Credit (:D) - grammies
*/

-- date encode the sample
create materialized view sampled_data_dates as
select
	row_index
	,"position"
	,track_name
	,artist
	,streams
	,url
	,TO_DATE(agg_date,'YYYY-MM-DD') as agg_date_parsed
	,region
from public.sampled_data
with data --- execution time of ~20 seconds
;

-- validation of date encoding
--- check encoding in random sample
--select * from sampled_data_dates order by random() limit 100;
--
-- evaluate max and min date encoding
--select 
--	min(agg_date_parsed) as min_date
--	,max(agg_date_parsed) as max_date
--from 
--sampled_data_dates


-- select data from neighbours
drop table if exists q3_neighbour_track_time_daily;
create table q3_neighbour_track_time_daily as
	select 
		*
	from
		sampled_data_dates
	where
		region in ('au','nz','gb','ie','ca','us','hk','fr')

-- calculate top tracks for neighbours over whole period - top 20 - starting with au nz - can leave this to Tableau I think

-- join on 
