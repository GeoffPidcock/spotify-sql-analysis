/* 
 QUESTION 1. 
 Do continents share same top ranking artists or songs? 

 Approach:
 This query approaches an answer through aggregating top 50 artists or tracks across each continent over the complete time period
 - rank() over partition by continent order by sum of streams
 - Tracks would add URL and track name, in addition to artist, in group by block
 - Have predicates to exclude null URL, and consider excluding incomplete country sets (or at the very least "global" and null regions)
 - union on a 'known' global comparison
 - Prepare results as tables for viz using Tableau 
 */

-- artists
drop table if exists q1_artist_top50;
create table q1_artist_top50 as
select 
	* 
from
	(
		-- compute continential aggregate
		select
			con.continent_name
			,raw.artist
			,sum(raw.streams) as sum_stream_count
			,rank() over (partition by con.continent_name order by sum(raw.streams) desc) as agg_position
		from
			public.data_raw raw inner join
			public.data_countries_continents con on con.country_code = raw.region 
		where
			raw.region not in ('ee','global','gr','gt','hn','hu','is','jp','lt','lu','lv','pa','py','sk','sv','uy') -- regions with incomplete data
			and raw.url is not null
		group by
			con.continent_name
			,raw.artist
		
		union
		
		-- compute global aggregate
		select
			'Global Comparison' as global_comp
			,raw.artist
			,sum(raw.streams) as sum_stream_count
			,rank() over (order by sum(raw.streams) desc) as agg_position
		from
			public.data_raw raw inner join
			public.data_countries_continents con on con.country_code = raw.region 
		where
			raw.region not in ('ee','global','gr','gt','hn','hu','is','jp','lt','lu','lv','pa','py','sk','sv','uy') -- regions with incomplete data
			and raw.url is not null
		group by
			global_comp
			,raw.artist
	) artist_rank
where artist_rank.agg_position <=50 -- only keep the top 50, of the 200

-- songs

drop table if exists q1_track_top50;
create table q1_track_top50 as
select 
	* 
from
	(
		-- compute continential aggregate
		select
			con.continent_name
			,raw.url
			,raw.track_name
			,raw.artist
			,sum(raw.streams) as sum_stream_count
			,rank() over (partition by con.continent_name order by sum(raw.streams) desc) as agg_position
		from
			public.data_raw raw inner join
			public.data_countries_continents con on con.country_code = raw.region 
		where
			raw.region not in ('ee','global','gr','gt','hn','hu','is','jp','lt','lu','lv','pa','py','sk','sv','uy') -- regions with incomplete data
			and raw.url is not null
		group by
			con.continent_name
			,raw.url
			,raw.track_name
			,raw.artist
		
		union
		
		-- compute global aggregate
		select
			'Global Comparison' as global_comp
			,raw.url
			,raw.track_name
			,raw.artist
			,sum(raw.streams) as sum_stream_count
			,rank() over (order by sum(raw.streams) desc) as agg_position
		from
			public.data_raw raw inner join
			public.data_countries_continents con on con.country_code = raw.region 
		where
			raw.region not in ('ee','global','gr','gt','hn','hu','is','jp','lt','lu','lv','pa','py','sk','sv','uy') -- regions with incomplete data
			and raw.url is not null
		group by
			global_comp
			,raw.url
			,raw.track_name
			,raw.artist
	) track_rank
where track_rank.agg_position <=50 -- only keep the top 50, of the 200
