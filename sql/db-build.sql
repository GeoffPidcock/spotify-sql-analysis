/* 
scope - builds database, table, and copies data into table 
v1 - gpidc 20190307
*/

CREATE DATABASE spotify;

SET CONNECTION to spotify;

-- Create Raw Data Tables

CREATE TABLE data_raw(
    row_index SERIAL,
    position int,
    track_name text,
    artist text,
    streams int,
    url text,
    agg_date text,
    region char varying(50)
);

CREATE TABLE data_countries_continents(
    country_code char varying(2) PRIMARY KEY,
    continent_code char varying(2),
    continent_name text
);

CREATE TABLE data_countries_location_lang(
	country_code char varying(2) PRIMARY KEY,
	country_name text,
	latitude numeric(10,6),
	longitude numeric(10,6),
	anglosphere boolean
);

-- Import Data

SET CLIENT_ENCODING TO 'utf8';

COPY  data_raw(position,track_name,artist,streams,url,agg_date,region)
FROM 'C:\Users\Geoff Pidcock\Anaconda_Projects\Other Projects\spotify-sql-analysis\data\data.csv'
DELIMITER ',' CSV header;

-- > This was executed using PSQL : \COPY  data_raw(position,track_name,artist,streams,url,agg_date,region) FROM 'C:\Users\Geoff Pidcock\Anaconda_Projects\Other Projects\spotify-sql-analysis\data\data.csv' DELIMITER ',' CSV header;

COPY data_countries_continents
FROM 'C:\Users\Geoff Pidcock\Anaconda_Projects\Other Projects\spotify-sql-analysis\data\countries_continents.csv'
DELIMITER ',' CSV header;

-- > This was executed using PSQL : \COPY  data_countries_continents FROM 'C:\Users\Geoff Pidcock\Anaconda_Projects\Other Projects\spotify-sql-analysis\data\countries_continents.csv' DELIMITER ',' CSV header;

COPY data_countries_location_lang
FROM 'C:\Users\Geoff Pidcock\Anaconda_Projects\Other Projects\spotify-sql-analysis\data\countries_codes_and_coordinates.csv'
DELIMITER ',' CSV header;

-- > This was executed using PSQL : \COPY  data_countries_location_lang FROM 'C:\Users\Geoff Pidcock\Anaconda_Projects\Other Projects\spotify-sql-analysis\data\countries_codes_and_coordinates.csv' DELIMITER ',' CSV header;