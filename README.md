# spotify-sql-analysis

ETL and analysis of spotify song ranking data using PostGreSQL

## Acknowledgements
- Tadas Tamošauskas for sharing countries with lats and longs - [link](https://gist.github.com/tadast/8827699)
- Wikipedia and Contributors for classifying countries into continents - [link](https://en.wikipedia.org/wiki/List_of_sovereign_states_and_dependent_territories_by_continent_(data_file))
- Environmental Systems Research , Inc. (ESRI), 20020401, World Continents: ESRI Data & Maps 2002, Environmental Systems Research Institute, Inc. (ESRI), Redlands, California, USA. [link](https://www.arcgis.com/home/item.html?id=5cf4f223c4a642eb9aa7ae1216a04372)